import React from 'react';
import { Switch, Route } from 'react-router-dom';
import StartButton from './modules/StartButton';
import Subscription from './modules/ContainerSubsciption/Subscription';

const App = () => (
  <Switch>
      <Route exact path='/' component={StartButton}/>
      <Route path='/subscription' component={Subscription}/>
  </Switch>
)

export default App



