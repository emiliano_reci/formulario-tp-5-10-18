import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './StartButton.css';

class StartButton extends Component{
    render(){
        return(
            <div>  
                <div className="shadow" id="btnContainer">
                        <Link 
                            to='/subscription'
                            className="link"
                        >
                            <p className="textBtnComenzar">COMENZAR SUSCRIPCION</p>
                        </Link>
                </div>  
            </div>
        )
    }
}

export default StartButton;