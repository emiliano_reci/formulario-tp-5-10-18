import React, { Component } from 'react';
import { CountryDropdown, RegionDropdown } from 'react-country-region-selector'; 

const style = {
    backgroundColor:'rgba(0, 0, 0, 0.3)',
    borderColor:'rgba(0, 0, 0, 0.3)',
    borderRadius: '10px',
    fontSize:'0.9em',
    color:'white',
}
class CountrySelection extends Component {
    //enviar pais al padre
    selectCountry (val) {
        this.props.fromParentCountry(val);
    }
    //enviar region al padre
    selectRegion (val) {
       this.props.fromParentRegion(val);
    }
    render(){
        return(        
            <div>
                <CountryDropdown
                    value={this.props.sendCountry}
                    onChange={(val) => this.selectCountry(val)} 
                    onClick={this.sendDataToParent} 
                    style={style}
                />
                <RegionDropdown
                    country={this.props.sendCountry}
                    value={this.props.sendRegion}
                    onChange={(val) => this.selectRegion(val)} 
                    onClick={this.sendDataToParent2}
                    style={style}
                />
            </div>
        )
    }
}
export default CountrySelection;