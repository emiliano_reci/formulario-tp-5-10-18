import React, { Component } from 'react';
import Cards from 'react-credit-cards';
import 'react-credit-cards/es/styles-compiled.css';
import CreditCardInput from 'react-credit-card-input';

const style ={
	backgroundColor:'rgba(0, 0, 0, 0.3)',
	borderColor:'rgba(0, 0, 0, 0.3)',
	borderRadius: '10px',
	fontSize:'0.8em',
	color:'white',
	lineHeight:'1.7em'
}

class CreditCard extends Component {
	//envio valores de los input de la tarjeta al padre, para setear su state
	sendDataNumber= (e) => {
		this.props.handlerDataNum(e)
    }
	sendDataExp= (e) => {
		this.props.handlerDataExp(e)
    }
	sendDataCvv= (e) => {
		this.props.handlerDataCvv(e)
	}
    render(){
        return(
            <div>
				<CreditCardInput 
					cardCVCInputRenderer={({ handleCardCVCChange, props }) => (
                        <input  style={style}
                            {...props}
							onChange={handleCardCVCChange((e) => this.sendDataCvv(e.target.value))}
							value={this.props.sendCvv}
						/>
					)}
					cardExpiryInputRenderer={({handleCardExpiryChange, props }) => (
						<input style={style}
	                		{...props}
							onChange={handleCardExpiryChange((e) => this.sendDataExp(e.target.value))} 
							value={this.props.sendExp}
						/>
					)}
					cardNumberInputRenderer={({handleCardNumberChange, props }) => (
						<input style={style}
							{...props}
							onChange={handleCardNumberChange((e) => this.sendDataNumber(e.target.value))}
							value={this.props.sendNum} 
						/>
					)}
				/> 
				<br /><br />
				<Cards
					name={this.props.sendName}
					number={this.props.sendNum}
					expiry={this.props.sendExp}
					cvc={this.props.sendCvv}
				/>
            </div>
        )
    }
}
export default CreditCard;





