import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import indigo from '@material-ui/core/colors/indigo'
import blueGrey from "@material-ui/core/colors/blueGrey";

const styles = {
  root: {
    color: blueGrey[50],
    "&$checked": {
        color: indigo[600],
    }
  },
  checked: {}
};

class TypeSuscription extends React.Component {
    sendPay= (e) => {
        this.props.handlerPay(e)
    }
  render() {
    const { classes } = this.props;
    return (
      <div>
          <label>Free</label>
        <Radio
            checked={this.props.tildar === 'free'} 
            onChange={(e) => { this.sendPay( e.target.value )} }
            value="free"
            name="freeSuscription"
            aria-label="A"
            classes={{
                root: classes.root,
            }}
        />
         <Radio
            checked={this.props.tildar === 'premium'} 
            onChange={(e) => { this.sendPay( e.target.value )} }
            value="premium"
            name="premiumSuscription"
            aria-label="C"
            classes={{
              root: classes.root,
              checked: classes.checked,
            }}
        />
        <label>Premium U$S 10</label>
      </div>
    );
  }
}

TypeSuscription.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TypeSuscription);

                        


