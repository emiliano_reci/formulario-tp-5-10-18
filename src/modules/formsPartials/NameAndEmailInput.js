import React from 'react';
import { TextField } from '@material-ui/core';
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
    container: {
      display: "flex",
      flexWrap: "wrap"
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: 300,
    },
    dense: {
      marginTop: 19
    },
    menu: {
      width: 200
    }
  });
  
class NameAndEmailInput extends React.Component {
    state = {
        name: null,
        email:null,
      };  
    handleChange = name => event => {
        this.setState({
          name: event.target.value
        });
         //---------forma antigua
         let campoEntrada= event.target.name
         if(campoEntrada==='fullName') {
             campoEntrada= event.target.value;
             this.props.handleName(campoEntrada)
         } 
    };
    handleChange1 = email => event => {
        this.setState({
          email: event.target.value
        }); 
             this.props.handleEmail(event.target.value)
    }; 
    errorName= () => {
        const nameRegex = /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/g;
        if(this.state.name != null){
            if(nameRegex.test(this.state.name) && this.state.name.length > 4 ){
                return false;
            }return true;
        }
    }
    errorEmail= () => {
        const emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
        if(this.state.email != null){
            if(emailRegex.test(this.state.email) ){
                return false;
            }return true;
        }
    }
    render() {
        const { classes } = this.props;
        
        return (
            <div>
                <h4>Suscripcion</h4>
                <form className={classes.container} noValidate autoComplete="off">
                    <TextField
                      id="standard-name"
                      label="Nombre y Apellido"
                      name="fullName"
                      className={classes.textField}
                      value={this.props.sendName}
                      onChange={this.handleChange("name")}
                      margin="normal"
                      error={this.state.name === "" || this.errorName()}
                      helperText={this.state.name === "" || this.errorName() ? 'Campo vacio/ incorrecto!' : ' '}
                    />
                    <br />
                     <TextField
                      id="standard-name1"
                      label="Email"
                      name="email"
                      className={classes.textField}
                      value={this.props.sendEmail}
                      onChange={this.handleChange1()}
                      margin="normal"
                      error={this.state.email === "" || this.errorEmail() }
                      helperText={this.state.email === "" || this.errorEmail() ? 'Campo vacio/ incorrecto!' : ' '}
                    />
                </form>
            </div>
        );
    }
}

NameAndEmailInput.propTypes = {
    classes: PropTypes.object.isRequired
  };
  
export default withStyles(styles)(NameAndEmailInput);
  











