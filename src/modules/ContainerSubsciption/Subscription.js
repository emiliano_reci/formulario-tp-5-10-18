import React, { Component } from 'react';
import './Subscription.css';
import { Grid, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import swal from 'sweetalert2'
//components
import NameAndEmailInput from '../formsPartials/NameAndEmailInput';
import CountrySelection  from '../formsPartials/CountrySelection';
import TypeSuscription from '../formsPartials/TypeSuscription';
import CreditCard from '../formsPartials/CreditCard';
import CircularProgress from '@material-ui/core/CircularProgress';
import yellow from '@material-ui/core/colors/yellow';
import { Link } from 'react-router-dom'

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

class Subscription extends Component {
  state = {
      fromChildName: '',
      fromChildEmail: '',
      nameLength: false,
      country: '',
      region: '',
      typeSuscription: 'free', 
      showCreditCard: false,
      cardNumber:'',
      cardExp: '',
      cardCvv: ''
    }; 
  //setea nombre recibido
  receiveName= (dato) => {
      this.setState({ 
          fromChildName: dato
      });
      if(dato.length >= 5) return this.setState({ nameLength: true }) 
  }
  //setea email recibido
  receiveEmail= (dato) => {
    this.setState({
      fromChildEmail: dato
    });
  }
  //seteo el pais recibido y limpio region por si cambia el pais
  handleCountry= (dato) => {
    this.setState( {
      country: dato, 
      region: '', 
    });
  }
  //seteo la ciudad recibida
  handleRegion = (ciudad) => {
    this.setState({  
      region: ciudad,
    });
  }
  //setea tipo de suscripcion y llama a fn toggle de la tarjeta
  receivePay= (dato) => {
    this.setState( {
      typeSuscription: dato
    });
    this.togglePremiumSuscription();
  }
  //toggle de tarjeta, si es pago muestra form de tarjeta
  togglePremiumSuscription = () => {
    const doesShow = this.state.showCreditCard;
    this.setState( { showCreditCard: !doesShow  } )
  }
  //setea tarjeta num recibida
  receiveNum= (da) => {
      this.setState( {
        cardNumber: da
      });
  }
  //setea tarjeta exp recibida
  receiveExp= (da) => {
    this.setState( {
      cardExp: da
    });
  }
  //setea tarjeta cvv recibida
  receiveCvv= (da) => {
    this.setState( {
    cardCvv: da
    });
  }
   //btn cancelar suscripcion
   clearForm = () => {
    console.log(this.state.typeSuscription)
    this.setState({typeSuscription:'free'})
    this.setState({fromChildName: ''});
    this.setState({fromChildEmail: ''});
    this.setState({country: ''});
    this.setState({region: ''});
    this.setState({cardNumber: ''});
    this.setState({cardExp: ''});
    this.setState({cardCvv: ''});
  }
//si envia la suscripcion como premium, hago ocultar la tarjeta luego
  checkFreeTypeSuscrip= () =>{
    this.setState({showCreditCard: false })  
  }

  //btn Suscribir valida campos, si corresponde llama a la fn callFetch
  submit= (e) => {
    let dataToConvertJson;
    if(this.state.typeSuscription ==='free'){
      console.log('opcion free');
      if(this.state.fromChildName.length <= 4 || this.state.fromChildEmail.length === 0 ||
        this.state.country.length === 0 || this.state.region.length === 0 || !this.state.nameLength){
          console.log('campos de free incompleto')
          swal({
            title: "Verifique que haya completado correctamente los campos de nombre/E-mail.", 
            background: 'rgba(200, 0, 0, 0.5)'
           });  
        }else{
          //-----------   probando regular expressions
          const nameRegex = /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/g;
          const emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
          if (emailRegex.test(this.state.fromChildEmail) && nameRegex.test(this.state.fromChildName)) {
            dataToConvertJson= {
              tipo: this.state.typeSuscription.toLowerCase(),
              nombre: this.state.fromChildName.toLowerCase(),
              email: this.state.fromChildEmail.toLowerCase(),
              pais: this.state.country.toLowerCase(),
              ciudad: this.state.region.toLowerCase()
            };
            console.log(dataToConvertJson);
            document.getElementById("spinner").style.display="block";
          } else {
            swal({
              title: "Verifique que haya completado correctamente los campos de nombre/E-mail.", 
              background: 'rgba(200, 0, 0, 0.5)'
             }); 
          }
        }
    }else{
      console.log('opcion premium')
      if(this.state.cardNumber.length === 0 || this.state.cardNumber.length <= 15 ||
        this.state.cardExp.length === 0  ||
        this.state.cardCvv.length === 0 || this.state.cardCvv.length < 3 ||
        this.state.fromChildName.length <= 4 || this.state.fromChildEmail.length === 0 ||
        this.state.country.length === 0 || this.state.region.length === 0 || !this.state.nameLength){ 
          console.log('campos de tarjeta incompleto')
          swal({
            title: "Verifique que haya completado correctamente todos los campos requeridos", 
            background: 'rgba(200, 0, 0, 0.5)'
          });  
      }else{
        //const REGEX= /^\d{4}([ \-]?)((\d{6}\1?\d{5})|(\d{4}\1?\d{4}\1?\d{4}))$/gm;
        //if (REGEX.test(creditCard) ){
          console.log('campos de tarjeta completo y valido')
          dataToConvertJson= {
            tipo: this.state.typeSuscription.toLowerCase(),
            nombre: this.state.fromChildName.toLowerCase(),
            email: this.state.fromChildEmail.toLowerCase(),
            pais: this.state.country.toLowerCase(),
            ciudad: this.state.region.toLowerCase(),
            cardNumber: this.state.cardNumber,
            cardExp: this.state.cardExp,
            cardCvv: this.state.cardCvv
          };
        console.log(dataToConvertJson);
        document.getElementById("spinner").style.display="block";
      }
    }
    if(dataToConvertJson != null){
      console.log('json esta completo')
      const myJSON = JSON.stringify(dataToConvertJson);
      this.callFetch(myJSON);
    }else{console.log('json null')}
  }
  //por medio de fetch envio los datos
  callFetch= (myJSON) => {
    fetch('https://server-subscripcion-jsbrbnwqfv.now.sh/subscripciones', {
        method: 'post',
        headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
        },
        body: myJSON  
        })  
      .then((response) => {
        return response.json();
      })
      .then((data) => { 
        document.getElementById("spinner").style.display="none";
        swal({
          title: "Suscripcion realizada exitosamente", 
          background: 'rgba(0, 128, 128, 0.4)',
          padding:'50px',
          timer:2500,
          showConfirmButton: false
        })  
            console.log('enviado!!!')    
            this.clearForm();
            this.checkFreeTypeSuscrip();
      })
      .catch((error) => {
        console.log(error, 'catch error');
      });        
  }
  render() { 
    const classes = this.props;
    let creditCardJsx = null;
    if (this.state.showCreditCard){
        creditCardJsx = (
            <div>
              <h5>
                Ha seleccionado version Premium 
              </h5>
              <CreditCard 
                handlerDataNum={this.receiveNum}
                handlerDataExp={this.receiveExp}
                handlerDataCvv={this.receiveCvv}
                sendName={this.state.fromChildName}
                sendNum={this.state.cardNumber}
                sendExp={this.state.cardExp}
                sendCvv={this.state.cardCvv}
              />
            </div>
        )
    }
    return (
      <div className="App">
          <Grid className="Container">          
            <Row>
              <Col lg={12}>
                <NameAndEmailInput        
                  handlerFromParant2={this.handleDataFullNameAndEmail} 
                  handleName={this.receiveName}
                  handleEmail={this.receiveEmail}
                  sendName={this.state.fromChildName}
                  sendEmail={this.state.fromChildEmail}                 
                /><br />
              </Col>
            </Row>
            <Row>
              <Col xs={12} md={12} lg={12} className="CountrySelection">
                <CountrySelection 
                  fromParentCountry={this.handleCountry}
                  fromParentRegion={this.handleRegion} 
                  sendCountry={this.state.country}
                  sendRegion={this.state.region}
                />  <br />
              </Col>
            </Row>
            <Row>
              <Col lg={12} className="typeSuscriptionStyle">
                <TypeSuscription 
                  tildar={this.state.typeSuscription}
                  handlerPay={this.receivePay}
                />  
              </Col>
            </Row>
            <Row>
              <Col lg={12}>
                { creditCardJsx }<br />
              </Col>
            </Row>
            <Row>
              <Col lg={2} mdOffset={4} lgOffset={4}>
                <div className="buttonCanSend color-1">
                  <Link 
                    className="linkCancel2"
                    to='/' 
                    onClick={ this.clearForm}
                  >
                      <p className="textoBtnCancel">
                        Cancel 
                      </p>
                  </Link>  
                </div>
              </Col>
              <Col lg={2} >
              <div 
                className="buttonCanSend color-2"
                onClick={this.submit} 
              >
                <p className="textoBtnSend">
                  Send
                </p>
              </div>
              </Col>
            </Row>
          </Grid>
          <br />
          <div id="spinner">
            <CircularProgress 
              className={classes.progress} 
              size={70}
              style={{  color: yellow[500]}}
            />
          </div>
      </div> 
    );  
  }
}

Subscription.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(Subscription);

